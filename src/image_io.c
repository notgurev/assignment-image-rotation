#include "image_io.h"
#include "file_io.h"
#include "util.h"

// error handling functions
static const char *const format_read_messages[] = {
        [FORMAT_READ_ERROR] = "Error reading format\n",
        [FORMAT_READ_INVALID_HEADER] = "Error reading format: invalid header\n",
        [FORMAT_READ_INVALID_TYPE] = "Error reading format: invalid format type\n",
        [FORMAT_READ_PREMATURE_END_OF_FILE] = "Error reading format: premature end of file\n",
        [FORMAT_READ_INVALID_BITS] = "Error reading format: invalid bits\n",
        [FORMAT_READ_INVALID_PADDING] = "Error reading format: invalid padding\n",
};

const char* format_read_message(enum format_read_status status) {
    return format_read_messages[status];
}

static const char *const format_write_messages[] = {
        [FORMAT_WRITE_ERROR] = "Error writing format\n",
        [FORMAT_WRITE_HEADER_ERROR] = "Error writing format: error writing header\n",
        [FORMAT_WRITE_PADDING_ERROR] = "Error writing format: error writing padding\n"
};

const char* format_write_message(enum format_write_status status) {
    return format_write_messages[status];
}

// open/close utility functions with error handling

static FILE *open(const char *filename, char *mode) {
    FILE *file = NULL;
    const enum open_status open_status = file_open(filename, &file, mode);
    if (open_status != OPEN_OK) err(open_message(open_status));
    return file;
}

static void close(FILE *file) {
    const enum close_status close_status = file_close(file);
    if (close_status != CLOSE_OK) err(close_message(close_status));
}

// functions used in main

struct image image_read(const char *filename, format_reader from_format) {
    FILE *file = open(filename, "rb");

    // read format
    struct image image = {0};
    const enum format_read_status format_read_status = from_format(file, &image);
    if (format_read_status != FORMAT_READ_OK) err(format_read_message(format_read_status));

    close(file);

    return image;
}

void image_save(const char *filename, const struct image image, format_writer to_format) {
    FILE *file = open(filename, "wb");

    // write format
    const enum format_write_status format_write_status = to_format(file, &image);
    if (format_write_status != FORMAT_WRITE_OK) err(format_write_message(format_write_status));

    close(file);
}