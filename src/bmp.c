#include "bmp.h"
#include "image_io.h"

#include <stdio.h>
#include <stdbool.h>

#define BMP 0x4d42

#define FOR_BMP_HEADER(FOR_FIELD) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD(t, n) t n ;

struct __attribute__((packed)) bmp_header {
    FOR_BMP_HEADER(DECLARE_FIELD)
};

static uint32_t calculate_padding(uint32_t width) {
    return (4 - width * 3 % 4) % 4;
}

static struct bmp_header bmp_header_create(uint32_t width, uint32_t height) {
    const uint32_t image_size = width * height * 3 + calculate_padding(width) * height;
    return (struct bmp_header) {
            .bfType = BMP,
            .bfileSize = sizeof(struct bmp_header) + image_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

static bool read_header(FILE *in, struct bmp_header *header) {
    const size_t count = 1;
    const size_t result = fread(header, sizeof(struct bmp_header), count, in);
    return result == count;
}

static bool write_header(FILE *out, const struct bmp_header *header) {
    const size_t count = 1;
    const size_t result = fwrite(header, sizeof(struct bmp_header), count, out);
    return result == count;
}

bool validate_header(struct bmp_header header, enum format_read_status *status) {
    if (header.bfType != BMP) {
        *status = FORMAT_READ_INVALID_TYPE;
        return false;
    }
    return true;
}

/* BMP handlers */
enum format_read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};

    if (!read_header(in, &header)) return FORMAT_READ_INVALID_HEADER;

    enum format_read_status header_validation_error;
    if (!validate_header(header, &header_validation_error)) {
        return header_validation_error;
    }

    *img = image_create(header.biWidth, header.biHeight);

    const uint32_t padding = calculate_padding(img->width);

    for (uint32_t row = 0; row < img->height; row++) {
        const int32_t count = fread(&img->data[row * img->width], sizeof(struct pixel), img->width, in);
        // check count
        if (count != img->width) {
            image_destroy(*img);
            return feof(in) ? FORMAT_READ_PREMATURE_END_OF_FILE : FORMAT_READ_INVALID_BITS;
        }
        // check padding
        if (fseek(in, padding, 1)) {
            image_destroy(*img);
            return FORMAT_READ_INVALID_PADDING;
        }
    }
    return FORMAT_READ_OK;
}

enum format_write_status to_bmp(FILE *out, const struct image *img) {
    const struct bmp_header header = bmp_header_create(img->width, img->height);

    if (!write_header(out, &header)) return FORMAT_WRITE_HEADER_ERROR;
    const uint32_t padding = calculate_padding(img->width);
    const int32_t padding_value = 0;

    for (uint32_t row = 0; row < img->height; row++) {
        const uint64_t count = fwrite(&img->data[row * img->width], sizeof(struct pixel), img->width, out);
        if (count != img->width) return FORMAT_WRITE_ERROR;
        // add padding
        const uint32_t padding_count = fwrite(&padding_value, 1, padding, out);
        if (padding_count != padding) return FORMAT_WRITE_PADDING_ERROR;
    }
    return FORMAT_WRITE_OK;
}