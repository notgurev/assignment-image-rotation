#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_IO_H

#include <stdio.h>
#include "image.h"

typedef enum format_read_status (*format_reader)(FILE *in, struct image *img); // => from_bmp()
typedef enum format_write_status (*format_writer)(FILE *out, const struct image *img); // => to_bmp()

enum format_read_status {
    FORMAT_READ_OK = 0,
    FORMAT_READ_ERROR,
    FORMAT_READ_INVALID_HEADER,
    FORMAT_READ_INVALID_TYPE,
    FORMAT_READ_PREMATURE_END_OF_FILE,
    FORMAT_READ_INVALID_BITS,
    FORMAT_READ_INVALID_PADDING
};

const char* format_read_message(enum format_read_status status);

enum format_write_status {
    FORMAT_WRITE_OK = 0,
    FORMAT_WRITE_ERROR,
    FORMAT_WRITE_HEADER_ERROR,
    FORMAT_WRITE_PADDING_ERROR
};

const char* format_write_message(enum format_write_status status);

struct image image_read(const char *filename, format_reader from_format);
void image_save(const char *filename, struct image image, format_writer to_format);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_IO_H
