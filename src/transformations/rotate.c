#include <stdint.h>
#include <image.h>
#include "rotate.h"

// Разумно сделать функцию, которая будет считать адрес пикселя по его координатам -- типа такого?
size_t index_by_coordinates(uint64_t source_height, uint64_t height, uint64_t width) {
    return source_height * width + (source_height - 1 - height);
}

struct image rotate(struct image image) {
    const uint64_t source_height = image.height;
    const uint64_t source_width = image.width;
    const struct image rotated = image_create(source_height, source_width);

    for (uint64_t h = 0; h < source_height; h++) {
        for (uint64_t w = 0; w < source_width; w++) {
            rotated.data[index_by_coordinates(source_height, h, w)] = image.data[source_width * h + w];
        }
    }
    return rotated;
}
