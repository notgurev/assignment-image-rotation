#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H

#include <stdio.h>
#include <errno.h>

enum open_status {
    OPEN_OK = 0,
    // example errors
    OPEN_FILE_NOT_FOUND = ENOENT
};

const char* open_message(enum open_status status);

enum close_status {
    CLOSE_OK = 0,
    // example errors
    CLOSE_MAX_SIZE_EXCEEDED = EFBIG,
    CLOSE_NO_SPACE = ENOSPC
};

const char* close_message(enum close_status status);

enum open_status file_open(const char *filename, FILE **f, char *mode);

enum close_status file_close(FILE *f);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H
