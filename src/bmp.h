#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "image.h"

// BMP handlers
enum format_read_status from_bmp(FILE *in, struct image *img);
enum format_write_status to_bmp(FILE *out, const struct image *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
