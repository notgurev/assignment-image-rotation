#include <stdint.h>
#include <stdlib.h>
#include "image.h"

struct image image_create(uint64_t width, uint64_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
}

void image_destroy(struct image image) {
    free(image.data);
}
