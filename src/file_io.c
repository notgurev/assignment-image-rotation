#include "file_io.h"
#include <errno.h>
#include <stdio.h>

enum open_status file_open(const char *filename, FILE **f, char *mode) {
    *f = fopen(filename, mode);
    return errno;
}

static const char *const open_messages[] = {
        [OPEN_FILE_NOT_FOUND] = "Error opening file: not found\n"
};

const char* open_message(enum open_status status) {
    return open_messages[status];
}

enum close_status file_close(FILE *f) {
    fclose(f);
    return errno;
}

static const char *const close_messages[] = {
        [CLOSE_MAX_SIZE_EXCEEDED] = "Error closing file: max size exceeded",
        [CLOSE_NO_SPACE] = "Error closing file: no disk space"
};

const char* close_message(enum close_status status) {
    return close_messages[status];
}