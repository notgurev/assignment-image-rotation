#include <stdio.h>
#include <transformations/rotate.h>
#include "util.h"
#include "image.h"
#include "image_io.h"
#include "bmp.h"

void usage() {
    fprintf(stderr, "Usage: ./rotate SRC_NAME NEW_NAME\n");
}

int main(int argc, char **argv) {
    if (argc != 3) usage();
    if (argc < 3) err("Not enough arguments \n");
    if (argc > 3) err("Too many arguments \n");

    const char *input_image_name = argv[1];
    const char *output_image_name = argv[2];

    // errors are handled inside image_read/save to remove a pointless layer of error handling

    const struct image image = image_read(input_image_name, from_bmp);

    const struct image rotated_image = rotate(image);

    image_save(output_image_name, rotated_image, to_bmp);

    // optional
    image_destroy(image);
    image_destroy(rotated_image);

    return 0;
}
