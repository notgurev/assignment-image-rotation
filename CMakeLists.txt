cmake_minimum_required(VERSION 3.17)
project(assignment_image_rotation C)

set(CMAKE_C_STANDARD 11)

include_directories(src)

add_executable(assignment_image_rotation
        src/image.c src/image.h src/bmp.c src/bmp.h src/main.c src/util.c src/util.h src/file_io.c src/file_io.h src/image_io.c src/image_io.h src/transformations/rotate.c src/transformations/rotate.h)
